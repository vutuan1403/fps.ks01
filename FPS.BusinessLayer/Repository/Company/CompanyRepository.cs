﻿using FPS.BusinessLayer.ViewModel.Company;
using FPS.DataLayer.Context;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FPS.BusinessLayer.Repository.Company
{
    public class CompanyRepository :BaseRepository, ICompanyRepository

    {
        public CompanyRepository(FPSContext context) : base(context)
        {
        }

        public void AddCompany(CompanyModel model)
        {
            FPS.DataLayer.Entity.Company company = new FPS.DataLayer.Entity.Company();
            company.Id = Guid.NewGuid();
            company.CompanyName = model.CompanyName;
            company.CreatedDate = DateTime.Now;
            company.UpdatedDate = DateTime.Now;
            company.CreatedBy = model.CreatedBy;
            company.UpdatedBy = model.UpdatedBy;
            company.IsDeleted = false;
            _context.Companies.Add(company);
            _context.SaveChanges();

        }
        public void DeleteCompany(Guid companyId)
        {
            var DeleteCompany = _context.Companies.FirstOrDefault(c => c.Id == companyId);
            if (DeleteCompany != null)
            {
                _context.Companies.Remove(DeleteCompany);
                _context.SaveChanges();
            }
        }
        public CompanyModel GetCompanyById(Guid companyId)
        {
            var company = _context.Companies.FirstOrDefault(c => c.Id == companyId);
            if (company != null)
            {
                return new CompanyModel
                {
                    CompanyName = company.CompanyName,
                    CreatedDate = company.CreatedDate,
                    UpdatedDate = company.UpdatedDate,
                    CreatedBy = company.CreatedBy,
                    UpdatedBy = company.UpdatedBy,
                    IsDeleted = company.IsDeleted,
                };
            }
            return null;
        }
        public IEnumerable<CompanyModel> GetAllCompanies()
        {
            return _context.Companies
                .Select(company => new CompanyModel
                {
                    CompanyName = company.CompanyName,
                    CreatedDate = company.CreatedDate,
                    UpdatedDate = company.UpdatedDate,
                    CreatedBy = company.CreatedBy,
                    UpdatedBy = company.UpdatedBy,
                    IsDeleted = company.IsDeleted
                })
                .ToList();
        }
        public void UpdateCompany(Guid companyId, CompanyModel model)
        {
            var existingCompany = _context.Companies.FirstOrDefault(c => c.Id == companyId);
            if (existingCompany != null)
            {
                existingCompany.CompanyName = model.CompanyName;
                existingCompany.UpdatedDate = DateTime.Now;
                existingCompany.UpdatedBy = model.UpdatedBy;

                _context.SaveChanges();
            }

        }
    }
}
